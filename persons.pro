#-------------------------------------------------
#
# Project created by QtCreator 2014-11-27T22:04:41
#
#-------------------------------------------------

QT       += core

QT       -= gui

TARGET = persons
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app


SOURCES += main.cpp \
    consoleui.cpp \
    personservice.cpp \
    person.cpp \
    personrepository.cpp

HEADERS += \
    consoleui.h \
    personservice.h \
    person.h \
    personrepository.h
