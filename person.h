#ifndef PERSON_H
#define PERSON_H

#include <string>
#include <algorithm>
#include <list>
using namespace std;

class Person
{
public:
    string firstName;
    string lastName;
    int yob;
    int yod;
    string sex;
    Person();
    Person(string _firstName, string _lastName, int _yob, int _yod, string _sex);


    bool sortbyname(const Person &lhs, const Person &rhs);
    bool sortbybirth(const Person &lhs, const Person &rhs);
    bool sortbydeath(const Person &lhs, const Person &rhs);
    bool sortbygender(const Person &lhs, const Person &rhs);

    bool operator <(const Person &p2) const;

};

#endif // PERSON_H
