#include "personservice.h"
#include "person.h"
#include "personrepository.h"

PersonService::PersonService()
{
    personRepo = PersonRepository();
}
void PersonService::add(Person p){
    personRepo.add(p);
}

list<Person> PersonService::display(char sortlist){
    return personRepo.display(sortlist);
}

Person PersonService::search(string find) {
    return personRepo.search(find);
}
