#include "consoleui.h"
using namespace std;

ConsoleUI::ConsoleUI()
{
    personService = PersonService();
}

void ConsoleUI::initial(){
    cout << endl;
    cout << "Write witch action you want to take; "<< endl;
    cout << endl;
    cout << "'add'   for adding a person "<< endl;
    cout << "'display'  to view the list"<< endl;
    cout << "'search' to search for a person by name" << endl;
    cout << "'q' to quit the program" << endl;
    cout << "'print' to print out list af is. "<< endl;

    cin >> input;
    //}while(input != "add" || input != "print" || input != "list");

}
void ConsoleUI::add(){

    Person p = Person();

    cout << "First name: ";
    cin >> p.firstName;
    cout << "Last name: ";
    cin >> p.lastName;
    cout << "Gender: ";
    cin >> p.sex;
    cout << "Year of birth: ";
    cin >> p.yob;
    cout << "Year of death: ";
    cin >> p.yod;
    personService.add(p);       //adds the person to the list
}

void ConsoleUI::display(){
    cout << "how would you like to sort the list?" << endl;
    cout << "'n' for name" << endl;
    cout << "'b' for year of birth" << endl;
    cout << "'d' for year of death" << endl;
    cout << "'g' for gender" << endl;
    cin >> sortlist;
    uipv = personService.display(sortlist);       //prints all scientist in the list and sorts them by user input

    for(list<Person>::iterator iter = uipv.begin(); iter != uipv.end(); iter++) {
        cout << iter->name << "\t" << iter->yob << "\t" << iter->yod << "\t" << iter->sex << endl;
    }
}

void ConsoleUI::search(){
    cout << "enter the name to search for: ";
    getline(cin, find);
    Person p;

    p = personService.search(find);
    while(p.firstName != ""){
        p = personService.search(find);
        cout << p.firstName << " " << p.lastName << p.yob << " " << p.yod << " " << p.sex << endl;
    }
}

void ConsoleUI::start(){
    cout << "Welcome!!\n" << endl;

    do{
        initial();
        if (input == "add"){
            system("CLS");
            add();
        }
        if(input == "list"){
            system("CLS");
            list();
        }
        if(input == "search"){
            search();
            system("CLS");
        }
        if(input == "print"){
            system("CLS");
        personService.personRepo.printOut();
        }
        else
            cout << "ERROR!!";

    }while(input != "q" && input != "Q");

}



