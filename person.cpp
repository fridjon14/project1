#include "person.h"

Person::Person()
{
    firstName = "";
    lastName = "";
    yob = -1;
    yod = -1;
    sex = "";
}
Person::Person(string _firstName, string _lastName, int _yob, int _yod, string _sex) {
    firstName = _firstName;
    lastName = _lastName;
    yob = _yob;
    yod = _yod;
    sex = _sex;
}



//bool Person::sortbyname(const Person &lhs, const Person &rhs) { return lhs.name < rhs.name; }

//bool Person::sortbybirth(const Person &lhs, const Person &rhs) { return lhs.yob < rhs.yob; }

//bool Person::sortbydeath(const Person &lhs, const Person &rhs) { return lhs.yod < rhs.yod; }

//bool Person::sortbygender(const Person &lhs, const Person &rhs) { return lhs.sex < rhs.sex; }

bool Person::operator <(const Person& p2) const {
    if(this->name < p2.name)
        return true;
    else
        return false;
}
