#ifndef PERSONSERVICE_H
#define PERSONSERVICE_H

#include <list>
#include "person.h"
#include "personrepository.h"
class PersonService
{
private:

public:
    PersonService();
    void add(Person p);
    void printOut();
    PersonRepository personRepo;
    list<Person> display(char);
    Person search(string);
};

#endif // PERSONSERVICE_H
