#ifndef PERSONREPOSITORY_H
#define PERSONREPOSITORY_H

#include <list>
#include <iostream>
#include <fstream>
#include <algorithm>
#include <string>
#include <iterator>
#include <iostream>
#include "person.h"
using namespace std;

class PersonRepository
{
private:
    list<Person> personlist;
public:
    PersonRepository();
    void add(Person p);
    void writeOut();

    list<Person> display(char);
    Person search(string);
    void readIn();

    friend ostream& operator<< (ostream&, const list<Person>& personlist);

};

#endif // PERSONREPOSITORY_H
