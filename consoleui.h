#ifndef CONSOLEUI_H
#define CONSOLEUI_H

#include <string>
#include <iostream>

#include "personservice.h"
#include "person.h"

using namespace std;

class ConsoleUI
{
private:

public:
    ConsoleUI();
    void start();
    void initial();
    void add();
    void display();

    PersonService personService;
    string input;
    Person p;
    char sortlist;
    string find;
    void search();
    list<Person> uipv;
};


#endif // CONSOLEUI_H
